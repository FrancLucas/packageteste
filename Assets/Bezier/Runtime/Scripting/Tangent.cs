﻿using UnityEngine;

namespace BezierHandler
{
    public enum ETangentType
    {
        Align, Vector, Free
    }

    [System.Serializable]
    public class Tangent
    {
        [SerializeField]
        private Vector3 position;
        [SerializeField]
        private ETangentType type;

        public Vector3 Position { get => position; internal set => position = value; }
        public ETangentType Type { get => type; set => type = value; }

        public void Align(Vector3 direction)
        {
            direction.Normalize();
            position = direction * position.magnitude;
        }

        public void Vector(IBezierLocal bezier, IBezierLocal otherBezier)
        {
            Vector3 position = bezier.Position;
            Vector3 otherPosition = otherBezier.Position;
            Vector3 direction = (otherPosition - position).normalized;

            this.position = direction * this.position.magnitude;
        }
    }
}
