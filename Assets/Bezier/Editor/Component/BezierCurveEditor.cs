﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using BezierHandler;
using System.Reflection;
using System;


[CustomEditor(typeof(BezierCurve))]
public class BezierCurveEditor : Editor
{
    public enum EBezierEditType { Bezier = 0, StartTangent = 1, EndTangent = 2 }

    private BezierCurve script;
    private FieldInfo beziersFieldInfo;

    private bool isDrawNormal;
    private bool isEdit;
    private EBezierEditType bezierEditType;

    private List<Bezier> beziers;
    private Bezier selectBezier;
    private bool IsSelectBezier => selectBezier != null;

    private int division;

    public bool IsEdit
    {
        get => isEdit;
        set
        {
            isEdit = Tools.hidden = value;
        }
    }

    private void OnEnable()
    {
        script = target as BezierCurve;
        beziersFieldInfo = script.GetType().GetField("beziers", BindingFlags.Instance | BindingFlags.NonPublic);
        beziers = (List<Bezier>)beziersFieldInfo.GetValue(script);

        division = 20;
        bezierEditType = EBezierEditType.Bezier;
        selectBezier = null;

        IsEdit = false;
    }

    private void OnDisable()
    {
        IsEdit = false;
    }

    public override void OnInspectorGUI()
    {
        if(GUILayout.Button("Edit"))
        {
            IsEdit = !IsEdit;
        }

        if(IsEdit)
        {
            CurveGUI();

            if(IsSelectBezier)
            {
                BezierGUI();
            }
        }

        serializedObject.ApplyModifiedProperties();

        DebugGUI();
    }

    private void CurveGUI()
    {
        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Curve Info", EditorStyles.boldLabel);

        if(GUILayout.Button("Add Bezier"))
        {
            IsEdit = !IsEdit;
        }
    }

    private void BezierGUI()
    {
        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Bezier Info", EditorStyles.boldLabel);

        var propertyType = typeof(Tangent).GetProperty("Type");
        TangentTypePopup(propertyType, selectBezier.StartTangent, "Start Tagent Type", "Set start tangent type");
        TangentTypePopup(propertyType, selectBezier.EndTangent, "End Tagent Type", "Set end tangent type");
    }

    private void DebugGUI()
    {
        EditorGUILayout.Space();
        EditorGUILayout.LabelField("Debug", EditorStyles.boldLabel);

        isDrawNormal = EditorGUILayout.Toggle("Show Normal", isDrawNormal);
        division = EditorGUILayout.IntSlider("Division", division, 0, 20);
    }

    #region OnSceneGUI
    private void OnSceneGUI()
    {
        Shortcut();
        DrawCurveBeziers();

        if(IsEdit)
        {
            foreach(var bezier in beziers)
            {
                if(IsSelectBezier && selectBezier.Equals(bezier))
                {
                    DrawSelectBezier(bezier);
                }
                else
                {
                    DrawSelectButtonBezier(bezier);
                }
            }
        }

        if(isDrawNormal)
        {
            var points = script.CalculateFast(division);

            foreach(var point in points)
            {
                float scale = Mathf.Clamp(HandleUtility.GetHandleSize(point.position) * .5f, .3f, .8f);

                Handles.color = Color.blue;
                Handles.DrawLine(point.position, point.position + point.Forward * scale);
                Handles.color = Color.red;
                Handles.DrawLine(point.position, point.position + point.Right * scale);
                Handles.color = Color.green;
                Handles.DrawLine(point.position, point.position + point.Up * scale);
            }
        }
    }

    private void Shortcut()
    {
        var currentEvent = Event.current;

        if(currentEvent.type == EventType.KeyUp)
        {
            switch(currentEvent.keyCode)
            {
                case KeyCode.D:
                    IsEdit = !IsEdit;
                    break;

                case KeyCode.A:
                    int bezerint = ((int) bezierEditType) + 1;

                    if(bezerint > 2)
                    {
                        bezerint = 0;
                    }

                    bezierEditType = (EBezierEditType) bezerint;
                    break;
            }
        }
    }

    private void DrawSelectBezier(IBezierWorld bezierWorld)
    {
        Handles.color = Color.blue;
        Handles.DrawDottedLine(bezierWorld.Position, bezierWorld.StartTangentPosition, 2);

        Handles.color = Color.green;
        Handles.DrawDottedLine(bezierWorld.Position, bezierWorld.EndTangentPosition, 2);

        if(bezierEditType == EBezierEditType.Bezier)
        {
            if(Tools.current == Tool.Move)
            {
                EditorGUI.BeginChangeCheck();
                Vector3 newPosition = Handles.PositionHandle(bezierWorld.Position, Quaternion.identity);

                if(EditorGUI.EndChangeCheck())
                {
                    Undo.RecordObject(target, "Bezer Position Changes");
                    bezierWorld.Position = newPosition;
                }
            }

            DrawButtonBezierEditType(bezierWorld, EBezierEditType.StartTangent);
            DrawButtonBezierEditType(bezierWorld, EBezierEditType.EndTangent);
        }

        if(Tools.current != Tool.Move)
        {
            bezierEditType = EBezierEditType.Bezier;
            return;
        }

        if(bezierEditType == EBezierEditType.StartTangent)
        {
            EditorGUI.BeginChangeCheck();
            Vector3 newPosition = Handles.PositionHandle(bezierWorld.StartTangentPosition, Quaternion.identity);

            if(EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(target, "Start Tangent Position Changes");
                bezierWorld.StartTangentPosition = newPosition;
            }

            DrawButtonBezierEditType(bezierWorld, EBezierEditType.Bezier);
            DrawButtonBezierEditType(bezierWorld, EBezierEditType.EndTangent);
        }

        if(bezierEditType == EBezierEditType.EndTangent)
        {
            EditorGUI.BeginChangeCheck();
            Vector3 newPosition = Handles.PositionHandle(bezierWorld.EndTangentPosition, Quaternion.identity);

            if(EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(target, "End Tangent Position Changes");
                bezierWorld.EndTangentPosition = newPosition;
            }

            DrawButtonBezierEditType(bezierWorld, EBezierEditType.Bezier);
            DrawButtonBezierEditType(bezierWorld, EBezierEditType.StartTangent);
        }
    }

    private void DrawButtonBezierEditType(IBezierWorld bezierWorld, EBezierEditType editType)
    {
        var worldPosition = (Vector3) BezierProperty(editType).GetValue(bezierWorld);
        var handleSize = BezierHandleSize(worldPosition, editType);

        Handles.color = BezierColor(editType);

        if(Handles.Button(worldPosition, Quaternion.identity, handleSize, handleSize, BezierHandleCap(editType)))
        {
            bezierEditType = editType;
        }
    }

    private Color BezierColor(EBezierEditType editType)
    {
        switch(editType)
        {
            case EBezierEditType.Bezier:
                return Color.red;
            case EBezierEditType.StartTangent:
                return Color.blue;
            case EBezierEditType.EndTangent:
                return Color.green;
            default:
                return Color.white;
        }
    }

    private PropertyInfo BezierProperty(EBezierEditType editType)
    {
        string propertyName;

        switch(editType)
        {
            case EBezierEditType.Bezier:
                propertyName = "Position";
                break;
            case EBezierEditType.StartTangent:
                propertyName = "StartTangentPosition";
                break;
            case EBezierEditType.EndTangent:
                propertyName = "EndTangentPosition";
                break;
            default:
                throw new NotImplementedException();
        }

        return typeof(IBezierWorld).GetProperty(propertyName);
    }

    private Handles.CapFunction BezierHandleCap(EBezierEditType editType)
    {
        switch(editType)
        {
            case EBezierEditType.Bezier:
                return Handles.SphereHandleCap;
            case EBezierEditType.StartTangent:
            case EBezierEditType.EndTangent:
                return Handles.DotHandleCap;
            default:
                throw new NotImplementedException();
        }
    }

    private float BezierHandleSize(Vector3 worldPosition, EBezierEditType editType)
    {
        float size = HandleUtility.GetHandleSize(worldPosition);

        switch(editType)
        {
            case EBezierEditType.Bezier:
                return size * .2f;
            case EBezierEditType.StartTangent:
            case EBezierEditType.EndTangent:
                return size * .05f;
            default:
                throw new NotImplementedException();
        }
    }

    private void DrawSelectButtonBezier(Bezier bezier)
    {
        IBezierWorld bezierData = bezier;

        Handles.color = Color.red;
        float handleSize = BezierHandleSize(bezierData.Position, EBezierEditType.Bezier);

        if(Handles.Button(bezierData.Position, Quaternion.identity, handleSize, handleSize, Handles.SphereHandleCap))
        {
            selectBezier = bezier;
            bezierEditType = EBezierEditType.Bezier;
            Repaint();
        }
    }

    private void DrawCurveBeziers()
    {
        foreach(var bezier in beziers)
        {
            if(bezier.NextBezier != null)
            {
                bool isBezierEquals = IsSelectBezier && bezier.Equals(selectBezier) || bezier.NextBezier.Equals(selectBezier);
                Color color = (isBezierEquals) ? Color.yellow : Color.white;

                DrawCurveBezier(bezier, bezier.NextBezier, color);
            }
        }
    }

    private void DrawCurveBezier(IBezierWorld bezier, IBezierWorld otherBezier, Color color)
    {
        Handles.DrawBezier(bezier.Position, otherBezier.Position, 
            bezier.StartTangentPosition, otherBezier.EndTangentPosition,
            color, null, 1);
    }
    #endregion

    private void TangentTypePopup(PropertyInfo propertyType, Tangent tangent, string name, string recordObjectName)
    {
        EditorGUI.BeginChangeCheck();
        var newType = (ETangentType) EditorGUILayout.EnumPopup(name, (ETangentType) propertyType.GetValue(tangent));

        if(EditorGUI.EndChangeCheck())
        {
            Undo.RecordObject(target, recordObjectName);
            propertyType.SetValue(tangent, newType);
        }
    }
}

/*
if(isNormal)
{
    Vector3 up = transform.up;
    Quaternion lastQuaternion = Quaternion.LookRotation(transform.forward, transform.up);
    Vector3 lastPoint = Vector3.zero;
    Vector3 lastUp = transform.up;

    bool isFirstBezier = true;

    for(int i = 0; i < beziers.Count - 1; i++)
    {
        Bezier bezier = beziers[i];

        IBezierWorld bezierWorld = bezier;
        IBezierWorld nextBezierWorld = bezier.NextBezier;
        float value = 1f / division;

        for(int j = 0; j < division; j++)
        {
            float stepCalculate = value * j;
            Vector3 point = BezierUtility.Cubic(bezierWorld.Position, bezierWorld.StartTangentPosition, nextBezierWorld.EndTangentPosition, nextBezierWorld.Position, stepCalculate);
            Vector3 nextPoint = BezierUtility.Cubic(bezierWorld.Position, bezierWorld.StartTangentPosition, nextBezierWorld.EndTangentPosition, nextBezierWorld.Position, stepCalculate + 0.01f);

            Vector3 forward = (nextPoint - point).normalized;
            Quaternion targetRotation;

            if(isFirstBezier)
            {
                targetRotation = Quaternion.LookRotation(forward, transform.up);
            }
            else
            {
                Vector3 originForward = lastQuaternion * Vector3.forward;
                Vector3 originRight = lastQuaternion * Vector3.right;
                Vector3 originUp = lastQuaternion * Vector3.up;

                targetRotation = Quaternion.LookRotation(forward, originUp);

                Vector3 projectUp = Vector3.ProjectOnPlane(forward, originRight);
                float angleX = Vector3.Angle(originForward, projectUp);

                if(angleX > 90)
                {
                    Matrix4x4 m = Matrix4x4.TRS(lastPoint, lastQuaternion, new Vector3(1, 1, 1));
                    Vector3 localPosition = m.inverse.MultiplyPoint3x4(point);
                    float lenght = Vector3.Scale(Vector3.right, localPosition).magnitude;

                    if(lenght < 0.05f)
                    {
                        targetRotation *= Quaternion.Euler(0, 0, 180);
                    }
                }

                lastUp = targetRotation * Vector3.up;
            }

            Handles.color = Color.blue;
            Handles.DrawLine(point, point + targetRotation * Vector3.forward);
            Handles.color = Color.green;
            Handles.DrawLine(point, point + targetRotation * Vector3.up);
            Handles.color = Color.red;
            Handles.DrawLine(point, point + targetRotation * Vector3.right);

            lastPoint = point;
            lastQuaternion = targetRotation;
        }

        isFirstBezier = false;
    }
}
*/
