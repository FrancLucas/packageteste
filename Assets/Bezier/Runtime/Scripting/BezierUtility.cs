﻿using UnityEngine;

namespace BezierHandler
{
    public static class BezierUtility
    {
        public static Vector3 Cubic(Bezier bezier, float t)
        {
            if(bezier.NextBezier != null)
            {
                return Cubic(bezier, bezier.NextBezier, t);
            }
            else
            {
                return Vector3.zero;
            }
        }

        public static Vector3 Cubic(IBezierWorld currentBezier, IBezierWorld nextBezier, float t)
        {
            return Cubic(currentBezier.Position, currentBezier.StartTangentPosition, nextBezier.EndTangentPosition, nextBezier.Position, t);
        }

        // formula: p0(1-t)³ + p1(1 - t)²3t + p2(1-t)3t² + t³p3, t <= 1 && t >= 0
        public static Vector3 Cubic(Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3, float t)
        {
            if(t == 0)
                return p0;
            if(t == 1)
                return p3;

            var onelessT = (1 - t);
            var onelessT_squared = onelessT * onelessT;
            var t_squared = t * t;

            return onelessT_squared * onelessT * p0 
                + 3 * onelessT_squared * t * p1 
                + 3 * onelessT * t_squared * p2
                + t_squared * t * p3;
        }
    }
}
