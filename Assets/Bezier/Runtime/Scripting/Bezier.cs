﻿using UnityEngine;
using System;

namespace BezierHandler
{
    [Serializable]
    public class Bezier: IBezierWorld, IBezierLocal
    {
        [SerializeField]
        private Vector3 localPosition;
        [SerializeField]
        private Tangent startTangent;
        [SerializeField]
        private Tangent endTangent;

        public Transform Parent { get; set; }
        public Tangent StartTangent { get => startTangent; private set => startTangent = value; }
        public Tangent EndTangent { get => endTangent; private set => endTangent = value; }

        public Bezier PreviousBezier { get; set; }
        public Bezier NextBezier { get; set; }

        Vector3 IBezierWorld.Position
        {
            get => Parent.TransformPoint(localPosition);
            set
            {
                localPosition = Parent.InverseTransformPoint(value);
                UpdateTangent();
                UpdateTangentNeighbors();
            }
        }

        Vector3 IBezierWorld.StartTangentPosition
        {
            get => Parent.TransformPoint(startTangent.Position + localPosition);
            set
            {
                startTangent.Position = Parent.InverseTransformPoint(value) - localPosition;
                ApplyTangent(startTangent, NextBezier, endTangent);
            }
        }

        Vector3 IBezierWorld.EndTangentPosition
        {
            get => Parent.TransformPoint(endTangent.Position + localPosition);
            set
            {
                endTangent.Position = Parent.InverseTransformPoint(value) - localPosition;
                ApplyTangent(endTangent, PreviousBezier, startTangent);
            }
        }

        Vector3 IBezierLocal.Position { get => localPosition; set { localPosition = value; } }
        Vector3 IBezierLocal.StartTangentPosition { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        Vector3 IBezierLocal.EndTangentPosition { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public Bezier(Transform parent)
        {
            Parent = parent;
            startTangent = new Tangent();
            endTangent = new Tangent();
        }

        public Bezier(Transform parent, Vector3 localPosition): this(parent)
        {
            this.localPosition = localPosition;
        }

        public void ApplyTangent(Tangent tangent, Bezier bezier, Tangent otherTangent)
        {
            if(tangent.Type == ETangentType.Vector && bezier != null)
            {
                tangent.Vector(this, bezier);
            }
            else if(tangent.Type == ETangentType.Align && otherTangent.Type != ETangentType.Align)
            {
                tangent.Align(otherTangent.Position * -1);
            }

            if(otherTangent.Type == ETangentType.Align)
            {
                otherTangent.Align(tangent.Position * -1);
            }
        }

        public void UpdateTangent()
        {
            ApplyTangent(startTangent, NextBezier, endTangent);
            ApplyTangent(endTangent, PreviousBezier, startTangent);
        }

        public void UpdateTangentNeighbors()
        {
            if(NextBezier != null)
            {
                NextBezier.UpdateTangent();
            }

            if(PreviousBezier != null)
            {
                PreviousBezier.UpdateTangent();
            }
        }
    }
}