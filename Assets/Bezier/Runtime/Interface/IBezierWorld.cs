﻿using UnityEngine;

namespace BezierHandler
{
    public interface IBezierWorld
    {
        Vector3 Position { get; set; }
        Vector3 StartTangentPosition { get; set; }
        Vector3 EndTangentPosition { get; set; }
    }
}
