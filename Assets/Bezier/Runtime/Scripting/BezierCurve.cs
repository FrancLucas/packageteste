﻿using System.Collections.Generic;
using UnityEngine;

namespace BezierHandler
{
    public class BezierCurve : MonoBehaviour
    {
        [SerializeField]
        private List<Bezier> beziers;

        public BezierCurve()
        {
            beziers = new List<Bezier>();
        }

        public void OnValidate()
        {
            var cacheTransform = transform;

            for(int index = 0; index < beziers.Count; index++)
            {
                var bezier = beziers[index];
                bezier.Parent = cacheTransform;

                var previousIndexBezier = index - 1;
                var nextIndexBezier = index + 1;

                var isHavePreviousBezier = previousIndexBezier >= 0;
                var isHaveNextBezier = nextIndexBezier < beziers.Count;

                if(isHavePreviousBezier)
                {
                    bezier.PreviousBezier = beziers[previousIndexBezier];
                }

                if(isHaveNextBezier)
                {
                    bezier.NextBezier = beziers[nextIndexBezier];
                }
            }
        }

        public BezierData[] CalculateFast(int division)
        {
            int curveCount = beziers.Count - 1;
            int pointCount = (curveCount * division) + 1;
            int currentIndexPoint = 0;

            float fractionRange = 1f / division;
            BezierData[] datas = new BezierData[pointCount];
            BezierData? lastBezierData = null;

            for(int index = 0; index < curveCount; index++)
            {
                Bezier bezier = beziers[index];
                bool isLastCurve = index == curveCount - 1;
                int totalStep = (isLastCurve) ? division + 1 : division;

                for(int step = 0; step < totalStep; currentIndexPoint++, step++)
                {
                    var range = fractionRange * step;

                    var pointPosition = BezierUtility.Cubic(bezier, range);
                    var closetPointPosition = BezierUtility.Cubic(bezier, range + 0.01f);

                    Quaternion rotation = CalculateRotation(pointPosition, closetPointPosition, lastBezierData);
                    datas[currentIndexPoint].position = pointPosition;
                    datas[currentIndexPoint].rotation = rotation;
                    lastBezierData = datas[currentIndexPoint];
                }
            }

            return datas;
        }

        private Quaternion CalculateRotation(Vector3 point, Vector3 closetPointPosition, BezierData? lastBezierData)
        {
            var forward = (closetPointPosition - point).normalized;

            if(lastBezierData.HasValue)
            {
                var pointData = lastBezierData.Value;
                Quaternion rotation = Quaternion.LookRotation(forward, pointData.Up);
                Vector3 projectUp = Vector3.ProjectOnPlane(forward, pointData.Right);
                float angle = Vector3.Angle(pointData.Forward, projectUp);

                if(angle > 90)
                {
                    Vector3 localPosition = pointData.Matrix.inverse.MultiplyPoint3x4(point);
                    float localDistanceOnXAxis = Vector3.Scale(Vector3.right, localPosition).magnitude;

                    if(localDistanceOnXAxis < 0.05f)
                    {
                        rotation *= Quaternion.Euler(0, 0, 180);
                    }
                }

                return rotation;
            }
            else
            {
                return Quaternion.LookRotation(forward, transform.up);
            }
        }

        private void Reset()
        {
            beziers.Clear();
            beziers.Add(new Bezier(transform));
            beziers.Add(new Bezier(transform, Vector3.forward));
        }
    }
}
