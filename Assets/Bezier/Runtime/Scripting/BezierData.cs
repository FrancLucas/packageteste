﻿using UnityEngine;

namespace BezierHandler
{
    [System.Serializable]
    public struct BezierData
    {
        public Vector3 position;
        public Quaternion rotation;

        public Vector3 Forward => rotation * Vector3.forward;
        public Vector3 Right => rotation * Vector3.right;
        public Vector3 Up => rotation * Vector3.up;

        public Matrix4x4 Matrix => Matrix4x4.TRS(position, rotation, Vector3.one);

        public BezierData(Vector3 position, Quaternion rotation)
        {
            this.position = position;
            this.rotation = rotation;
        }
    }
}
